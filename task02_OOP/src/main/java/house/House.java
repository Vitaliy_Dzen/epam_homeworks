package house;

public abstract class House {

  private short id = 0;
  private static short countOfObject = 0;
  private String name;
  private double distanceToSocialInfrastructure;
  private double price;
  private String status;

  public House() {
    countOfObject++;
    id = countOfObject;
  }

  public House(String name, double distanceToSocialInfrastructure, double price,
      Enum status) {
    countOfObject++;
    id = countOfObject;
    this.name = name;
    this.distanceToSocialInfrastructure = distanceToSocialInfrastructure;
    this.price = price;
    this.status = status.toString();
  }

  public short getId() {
    return id;
  }

  public static short getCountOfObject() {
    return countOfObject;
  }

  public String getName() {
    return name;
  }

  public double getDistanceToSocialInfrastructure() {
    return distanceToSocialInfrastructure;
  }

  public double getPrice() {
    return price;
  }

  public String getStatus() {
    return status;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDistanceToSocialInfrastructure(double distanceToSocialInfrastructure) {
    this.distanceToSocialInfrastructure = distanceToSocialInfrastructure;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  abstract public String getType();

  @Override
  public String toString() {
    return "House{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", distanceToSocialInfrastructure=" + distanceToSocialInfrastructure +
        ", price=" + price +
        ", status='" + status + '\'';
  }
}
