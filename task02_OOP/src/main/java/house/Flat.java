package house;

public class Flat extends House {

  private String type = Type.FLAT.toString();

  public Flat() {
  }

  public Flat(String name, double distanceToSocialInfrastructure, double price,
      Enum status) {
    super(name, distanceToSocialInfrastructure, price, status);
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return super.toString() + ", type='" + type + '\'' + '}';
  }
}
