package house;

public class Penthouse extends House {

  private String type = Type.PENTHOUSE.toString();

  public Penthouse() {
  }

  public Penthouse(String name, double distanceToSocialInfrastructure, double price,
      Enum status) {
    super(name, distanceToSocialInfrastructure, price, status);
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return super.toString() + ", type='" + type + '\'' + '}';
  }
}
