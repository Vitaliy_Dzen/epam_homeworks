package house;

public class Bungalow extends House {

  private String type = Type.BUNGALOW.toString();

  public Bungalow() {
  }

  public Bungalow(String name, double distanceToSocialInfrastructure, double price,
      Enum status) {
    super(name, distanceToSocialInfrastructure, price, status);
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return super.toString() + ", type='" + type + '\'' + '}';
  }
}
