package house;

public class Mansion extends House {

  private String type = Type.MANSION.toString();

  public Mansion() {
  }

  public Mansion(String name, double distanceToSocialInfrastructure, double price,
      Enum status) {
    super(name, distanceToSocialInfrastructure, price, status);
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return super.toString() + ", type='" + type + '\'' + '}';
  }
}
