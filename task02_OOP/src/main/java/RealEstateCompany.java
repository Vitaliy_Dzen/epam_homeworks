import house.House;
import java.util.List;
import java.util.stream.Collectors;

public class RealEstateCompany {

  private List<House> houses;

  public RealEstateCompany(List<House> houses) {
    this.houses = houses;
  }

  public void printAll() {
    houses.forEach(System.out::println);
  }

  public static void printAll(List<House> collection) {
    collection.forEach(System.out::println);
  }

  public List<House> findHouseDistanceToSocialInfrastructureLess(final double distance) {
    return houses.stream().filter(p -> p.getDistanceToSocialInfrastructure() <= distance)
        .collect(Collectors.toList());
  }

  public List<House> findHousePriceLess(final double price) {
    return houses.stream().filter(p -> p.getPrice() <= price).collect(Collectors.toList());
  }

  public List<House> findHouseByType(final Enum inputEnum) {
    return houses.stream().filter(p -> p.getType().equals(inputEnum.toString()))
        .collect(Collectors.toList());
  }

  public List<House> findHouseByStatus(final Enum status) {
    return houses.stream().filter(p -> p.getStatus().equals(status.toString()))
        .collect(Collectors.toList());
  }

  public List<House> findHouseByName(final String name) {
    return houses.stream().filter(p -> p.getName().equals(name))
        .collect(Collectors.toList());
  }

  public List<House> sortByDistanceAndPrice(final double distance, final double price) {
    return houses.stream().filter(
        p -> p.getPrice() <= price && p.getDistanceToSocialInfrastructure() <= distance)
        .collect(Collectors.toList());
  }

}
