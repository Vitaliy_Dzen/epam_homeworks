import house.Bungalow;
import house.Flat;
import house.House;
import house.Mansion;
import house.Penthouse;
import house.Status;
import house.Type;
import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String[] args) {

    List<House> houses = new ArrayList();
    houses.add(new Mansion("h2", 99.2, 99, Status.NEW));
    houses.add(new Flat("h1", 12.2, 45, Status.NEW));
    houses.add(new Bungalow("h3", 45.2, 78.2, Status.OLD));
    houses.add(new Flat("h5", 120.2, 450, Status.NEW));
    houses.add(new Penthouse("h2", 12.0, 712, Status.OLD));

    RealEstateCompany realEstateCompany = new RealEstateCompany(houses);

    System.out.println("Print All");
    realEstateCompany.printAll();
    System.out.println();

    System.out.println("By distance < X");
    RealEstateCompany.printAll(realEstateCompany.findHouseDistanceToSocialInfrastructureLess(50));
    System.out.println();

    System.out.println("By type");
    RealEstateCompany.printAll(realEstateCompany.findHouseByType(Type.FLAT));
    System.out.println();

    System.out.println("By Price < X");
    RealEstateCompany.printAll(realEstateCompany.findHousePriceLess(150));
    System.out.println();

    System.out.println("By distance ant price < X");
    RealEstateCompany.printAll(realEstateCompany.sortByDistanceAndPrice(50, 100));
    System.out.println();

    System.out.println("By status");
    RealEstateCompany.printAll(realEstateCompany.findHouseByStatus(Status.OLD));
    System.out.println();

    System.out.println("By name");
    RealEstateCompany.printAll(realEstateCompany.findHouseByName("h2"));
  }
}
