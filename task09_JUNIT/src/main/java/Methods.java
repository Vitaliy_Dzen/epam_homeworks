public class Methods {

  public static double getAverage(double a, double b) {
    return (a + b) / 2;
  }

  public static double getSum(double a, double b) {
    return a + b;
  }

  public static double getDifference(double a, double b) {
    return a - b;
  }

  public static double getValueInDegree(double a, int b) {
    return Math.pow(a, b);
  }

  public static double getValueInSquareRoot(double a) {
    return Math.sqrt(a);
  }

}
