import static org.junit.jupiter.api.Assertions.*;

class MethodsTest {

  @org.junit.jupiter.api.Test
  void getAverage() {
    double actual = Methods.getAverage(45, 18);
    double expected = 31.5;
    assertEquals(expected, actual);
  }

  @org.junit.jupiter.api.Test
  void getSum() {
    double actual = Methods.getSum(45, 18);
    double expected = 63;
    assertEquals(expected, actual);
  }

  @org.junit.jupiter.api.Test
  void getDifference() {
    double actual = Methods.getDifference(45, 18);
    double expected = 27;
    assertEquals(expected, actual);
  }

  @org.junit.jupiter.api.Test
  void getValueInDegree() {
    double actual = Methods.getValueInDegree(45, 3);
    double expected = 91125;
    assertEquals(expected, actual);
  }

  @org.junit.jupiter.api.Test
  void getValueInSquareRoot() {
    double actual = Methods.getValueInSquareRoot(81);
    double expected = 9;
    assertEquals(expected, actual);
  }
}