package tasks;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class Task4 {

  public static void startTask() {

    Scanner scanner = new Scanner(System.in);

    StringBuffer stringBuffer = new StringBuffer();
    Boolean aBoolean = true;

    while (aBoolean) {
      String s = scanner.nextLine();
      if (s.equals("")) {
        aBoolean = false;
      }
      stringBuffer.append(s + " ");
    }
    Set<String> setOfWord = Arrays.stream(stringBuffer.toString().split(" ")).collect(Collectors.toSet());
    setOfWord.remove("");
    System.out.println("Number of unique words: " + setOfWord.size());

    for(String i : setOfWord){
      System.out.println(i+" | "+ountOfSymbol(i));
    }



  }

  private static String ountOfSymbol(String inputString) {
    inputString = inputString.replace(" ", "");
    Map<Character, Integer> numChars = new HashMap<>();
    for (int i = 0; i < inputString.length(); i++) {
      char charAt = inputString.charAt(i);

      if (!numChars.containsKey(charAt)) {
        numChars.put(charAt, 1);
      } else {
        numChars.put(charAt, numChars.get(charAt) + 1);
      }
    }
    return numChars.toString();
    }
  }
