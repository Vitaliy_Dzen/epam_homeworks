package tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Task3 {

  private static List<Integer> integerList = new ArrayList(10);

  public static void startTask() {
    generateRandomList();
    System.out.println("List:\n" + integerList);
    System.out.println("Max value: " + getMax());
    System.out.println("Min value: " + getMin());
    System.out.println("Average value: " + getAverage());
    System.out.println("Sum: " + getSum());
    System.out.println("Count number of values that are bigger than average: "+getCountOfValuesBiggerAverage());
  }

  private static void generateRandomList() {
    for (int i = 0; i < 10; i++) {
      integerList.add((int) (Math.random() * 100));
    }
  }

  private static int getMax() {
    return integerList
        .stream()
        .mapToInt(v -> v)
        .max().orElseThrow(NoSuchElementException::new);
  }

  private static int getMin() {
    return integerList
        .stream()
        .mapToInt(v -> v)
        .min().orElseThrow(NoSuchElementException::new);
  }

  private static double getAverage() {
    return integerList
        .stream()
        .mapToInt(v -> v)
        .average().orElse(0.0);
  }

  private static int getSum() {
    return integerList
        .stream()
        .mapToInt(Integer::intValue)
        .sum();
  }

  private static int getCountOfValuesBiggerAverage() {
    int i = 0;
    for (Integer n : integerList) {
      if (n > getAverage()) {
        i++;
      }
    }
    return i;
  }

}
