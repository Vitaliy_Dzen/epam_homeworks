package tasks;

import static sun.swing.MenuItemLayoutHelper.max;

public class Task1 {

  public static void startTask() {
    MyLambda maxValue = (a, b, c) -> max(a, b, c);
    MyLambda averageValue = (a, b, c) -> (a + b + c) / 3;
    System.out.println("Max value " + maxValue.fun1(11, 20, 3));
    System.out.println("Average value " + averageValue.fun1(11, 20, 3));
  }

  @FunctionalInterface
  interface MyLambda {

    int fun1(int a, int b, int c);
  }

}

