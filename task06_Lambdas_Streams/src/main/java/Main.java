import tasks.Task1;
import tasks.Task2;
import tasks.Task3;
import tasks.Task4;

public class Main {

  public static void main(String[] args) {

    Task1.startTask();
    Task2.startTask();
    Task3.startTask();
    Task4.startTask();
  }
}
