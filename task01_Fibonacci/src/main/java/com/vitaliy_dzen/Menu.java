/*
 *Menu
 *
 * v1.2
 *
 *  This is a class with menu of user
 */
package com.vitaliy_dzen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class Menu
 */
public class Menu {

  /**
   * Print menu items
   */
  public static void printMenu() {
    System.out.println("Menu:");
    System.out.println("Enter 1 to enter the interval (for example: [1;100]);");
    System.out.println("Enter 2 to print odd numbers from start to the end of interval");
    System.out.println("Enter 3 to print even numbers from end to the start of interval");
    System.out.println("Enter 4 to print the sum of odd numbers");
    System.out.println("Enter 5 to print the sum of even numbers");
    System.out.println("Enter 6 to enter size of Fibonacci");
    System.out.println("Enter 7 to print Fibonacci");
    System.out.println("Enter 8 to print the biggest odd numbers of Fibonacci");
    System.out.println("Enter 9 to print the biggest even numbers of Fibonacci");
    System.out.println("Enter 10 to print percentage of odd numbers of Fibonacci");
    System.out.println("Enter 11 to print percentage of even numbers of Fibonacci");
    System.out.println("Enter 0 to exit");
  }

  /**
   * Start menu
   */
  public static void menu() {
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    try {

      boolean isExit = false;
      String scanFirstLine = "";
      int startInterval = 0;
      int stopInterval = 0;
      int fibonacciSize = 0;

      printMenu();

      while (!isExit) {
        switch (Integer.parseInt(bufferedReader.readLine())) {
          case 0:
            isExit = true;
            break;
          case 1:
            System.out.println("Enter the interval (for example: [1;100]);");
            scanFirstLine = bufferedReader.readLine();
            startInterval = Integer
                .parseInt(scanFirstLine.substring(1, scanFirstLine.indexOf(";")));
            stopInterval = Integer.parseInt(
                scanFirstLine
                    .substring(scanFirstLine.indexOf(";") + 1, scanFirstLine.indexOf("]")));
            break;
          case 2:
            if (startInterval == 0 && stopInterval == 0) {
              System.out.println("ERROR. Enter interval");
            } else {
              System.out.println(
                  "Print odd numbers from start to the end of interval - " + Task
                      .getNumbersDecrease(
                          startInterval, stopInterval));
            }
            break;
          case 3:
            if (startInterval == 0 && stopInterval == 0) {
              System.out.println("ERROR. Enter interval");
            } else {
              System.out.println(
                  "Print even numbers from end to the start of interval - " + Task.getNumbersGrowth(
                      startInterval, stopInterval));
            }
            break;
          case 4:
            if (startInterval == 0 && stopInterval == 0) {
              System.out.println("ERROR. Enter interval");
            } else {
              System.out.println(
                  "Print the sum of odd numbers - " + Task.getSummOfEvenNumbers(startInterval,
                      stopInterval));
            }
            break;
          case 5:
            if (startInterval == 0 && stopInterval == 0) {
              System.out.println("ERROR. Enter interval");
            } else {
              System.out.println(
                  "The sum of even numbers - " + Task
                      .getSummOfOddNumbers(startInterval, stopInterval));
            }
            break;
          case 6:
            System.out.println("Enter size of Fibonacci");
            fibonacciSize = Integer.parseInt(bufferedReader.readLine());
            break;
          case 7:
            if (fibonacciSize == 0) {
              System.out.println("ERROR. Enter Fibonacci");
            } else {
              System.out.println("Fibonacci:");
              for (int i = 1; i <= fibonacciSize; i++) {
                System.out.println(i + ": " + Task.fibonacci(i));
              }
            }
            break;
          case 8:
            if (fibonacciSize == 0) {
              System.out.println("ERROR. Enter Fibonacci");
            } else {
              System.out.println(
                  "The biggest odd numbers of Fibonacci - " + Task.getOddFibonacci(fibonacciSize));
            }
            break;
          case 9:
            if (fibonacciSize == 0) {
              System.out.println("ERROR. Enter Fibonacci");
            } else {
              System.out.println(
                  "The biggest even numbers of Fibonacci - " + Task
                      .getEvenFibonacci(fibonacciSize));
            }
            break;
          case 10:
            if (fibonacciSize == 0) {
              System.out.println("ERROR. Enter Fibonacci");
            } else {
              System.out.println(
                  "Percentage of odd numbers of Fibonacci - " + Task.percentageOfFibonacciEven(
                      fibonacciSize));
            }
            break;
          case 11:
            if (fibonacciSize == 0) {
              System.out.println("ERROR. Enter Fibonacci");
            } else {
              System.out.println(
                  "Percentage of even numbers of Fibonacci - " + Task.percentageOfFibonacciOdd(
                      fibonacciSize));
            }
            break;
          default:
            System.out.println("You entered wrong number.  ");
            System.out.println();
            printMenu();
            break;
        }
      }
    } catch (IOException e) {
      System.err.println("An IOException was caught :" + e.getMessage());
      Menu.menu();
    } catch (NumberFormatException e) {
      System.err.println("An NumberFormatException was caught :" + e.getMessage());
      Menu.menu();
    } catch (Exception e) {
      System.err.println("An Exception was caught :" + e.getMessage());
      Menu.menu();
    }
  }
}
