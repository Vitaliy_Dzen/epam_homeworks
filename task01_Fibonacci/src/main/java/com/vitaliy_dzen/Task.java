/*
 *Task
 *
 *v1.2
 *
 * This is a class with tasks
 */
package com.vitaliy_dzen;

/**
 * Class Task
 */
public class Task {

  /**
   *Get odd numbers of interval in descending order
   *
   * @param startInterval first index of interval
   * @param stopInterval  last index of interval
   * @return  String of odd numbers of interval in descending order
   */
  public static String getNumbersDecrease(final int startInterval, final int stopInterval) {
    String outputData = "";
    for (int i = startInterval; i <= stopInterval; i++) {
      if (i % 2 != 0) {
        outputData += i + " ";
      }
    }
    return outputData;
  }

  /**
   *Get even numbers of interval in ascending order
   *
   * @param startInterval first index of interval
   * @param stopInterval  last index of interval
   * @return  String of even numbers of interval in ascending order
   */
  public static String getNumbersGrowth(final int startInterval, final int stopInterval) {
    String outputData = "";
    for (int i = stopInterval; i >= startInterval; i--) {
      if (i % 2 == 0) {
        outputData += i + " ";
      }
    }
    return outputData;
  }

  /**
   * Get sum of odd numbers
   *
   * @param startInterval first index of interval
   * @param stopInterval  last index of interval
   * @return  Sum of odd numbers
   */
  public static int getSummOfEvenNumbers(final int startInterval, final int stopInterval) {
    int summOfEvenNumbers = 0;
    for (int i = startInterval; i <= stopInterval; i++) {
      if (i % 2 == 0) {
        summOfEvenNumbers += i;
      }
    }
    return summOfEvenNumbers;
  }

  /**
   * Get sum of even numbers
   *
   * @param startInterval first index of interval
   * @param stopInterval  last index of interval
   * @return  Sum of even numbers
   */
  public static int getSummOfOddNumbers(final int startInterval, final int stopInterval) {
    int summOfOddNumbers = 0;
    for (int i = startInterval; i <= stopInterval; i++) {
      if (i % 2 != 0) {
        summOfOddNumbers += i;
      }
    }
    return summOfOddNumbers;
  }

  /**
   *Get value of Fibonacci of index
   *
   * @param n Index of Fibonacci
   * @return  Value of Fibonacci of index
   */
  public static int fibonacci(final int n) {
    if (n <= 1) {
      return n;
    } else {
      return fibonacci(n - 1) + fibonacci(n - 2);
    }
  }

  /**
   * Get the biggest odd numbers of Fibonacci
   *
   * @param fibonacciSize Size of Fibonacci
   * @return  The biggest odd numbers of Fibonacci
   */
  public static int getOddFibonacci(final int fibonacciSize) {
    int outputData = 0;
    for (int i = 1; i <= fibonacciSize; i++) {
      if (fibonacci(i) % 2 != 0) {
        outputData = fibonacci(i);
      }
    }
    return outputData;
  }

  /**
   * Get the biggest even numbers of Fibonacci
   *
   * @param fibonacciSize Size of Fibonacci
   * @return  The biggest even numbers of Fibonacci
   */
  public static int getEvenFibonacci(final int fibonacciSize) {
    int outputData = 0;
    for (int i = 1; i <= fibonacciSize; i++) {
      if (fibonacci(i) % 2 == 0) {
        outputData = fibonacci(i);
      }
    }
    return outputData;
  }

  /**
   * Get percentage of odd numbers of Fibonacci
   *
   * @param fibonacciSize Size of Fibonacci
   * @return Percentage of odd numbers of Fibonacci
   */
  public static double percentageOfFibonacciEven(final int fibonacciSize) {
    int countOfEvenFibonacci = 0;
    for (int i = 1; i <= fibonacciSize; i++) {
      if (fibonacci(i) % 2 != 0) {
        countOfEvenFibonacci++;
      }
    }
    return (double) (countOfEvenFibonacci * 100) / fibonacciSize;
  }

  /**
   * Get percentage of even numbers of Fibonacci
   *
   * @param fibonacciSize Size of Fibonacci
   * @return  Percentage of even numbers of Fibonacci
   */
  public static double percentageOfFibonacciOdd(final int fibonacciSize) {
    int countOfOddFibonacci = 0;
    for (int i = 1; i <= fibonacciSize; i++) {
      if (fibonacci(i) % 2 == 0) {
        countOfOddFibonacci++;
      }
    }
    return (double) (countOfOddFibonacci * 100) / fibonacciSize;
  }
}
