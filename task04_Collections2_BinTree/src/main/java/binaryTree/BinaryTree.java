package binaryTree;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {

  Node root;

  private Node addRecursive(Node current, int value, String data) {
    if (current == null) {
      return new Node(value, data);
    }

    if (value < current.value) {
      current.left = addRecursive(current.left, value, data);
    } else if (value > current.value) {
      current.right = addRecursive(current.right, value, data);
    } else {
      return current;
    }
    return current;
  }

  public void put(int value, String data) {
    root = addRecursive(root, value, data);
  }

  private Node deleteRecursive(Node current, int value) {
    if (current == null) {
      return null;
    }

    if (value == current.value) {

      if (current.left == null && current.right == null) {
        return null;
      }
      if (current.right == null) {
        return current.left;
      }
      if (current.left == null) {
        return current.right;
      }

      int smallestValue = findSmallestValue(current.right);
      current.value = smallestValue;
      current.right = deleteRecursive(current.right, smallestValue);
      return current;
    }

    if (value < current.value) {
      current.left = deleteRecursive(current.left, value);
      return current;
    }
    current.right = deleteRecursive(current.right, value);
    return current;
  }

  private int findSmallestValue(Node root) {
    return root.left == null ? root.value : findSmallestValue(root.left);
  }

  public void delete(int value) {
    root = deleteRecursive(root, value);
  }


  public void traversePreOrder(Node node) {
    if (node != null) {
      System.out.println("Value - " + node.value + ", data - " +node.data);
      traversePreOrder(node.left);
      traversePreOrder(node.right);
    }
  }

    public void traverseInOrder(Node node) {
        if (node != null) {
            traverseInOrder(node.left);
            System.out.print(" " + node.value);
            traverseInOrder(node.right);
        }
    }

    public void traversePostOrder(Node node) {
        if (node != null) {
            traversePostOrder(node.left);
            traversePostOrder(node.right);
            System.out.print(" " + node.value);
        }
    }

  public void print() {
    traversePreOrder(root);
  }


}