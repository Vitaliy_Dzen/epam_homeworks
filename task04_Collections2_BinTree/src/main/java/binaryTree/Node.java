package binaryTree;

class Node {
    int value;
    String data;
    Node left;
    Node right;

    Node(int value, String data) {
        this.value = value;
        this.data = data;
        right = null;
        left = null;
    }


}