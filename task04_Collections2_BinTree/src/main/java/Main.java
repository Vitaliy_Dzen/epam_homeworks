import binaryTree.*;

public class Main {

  public static void main(String[] args) {

      BinaryTree binaryTree = new BinaryTree();

    binaryTree.put(456, "s456s");
    binaryTree.put(0, "Q0Q");
    binaryTree.put(78, "String");
    binaryTree.put(9, "Hello World");
    binaryTree.put(460, "x460x");

    binaryTree.print();
    System.out.println("**************************");

    binaryTree.delete(0);
    binaryTree.delete(456);
    binaryTree.print();

    System.out.println("**************************");
    binaryTree.put(0, "D000D");
    binaryTree.delete(78);
    binaryTree.print();


    }


  }