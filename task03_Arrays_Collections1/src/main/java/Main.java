import static task2.Task2.dellEqualNumbers;
import static task2.Task2.difference;
import static task2.Task2.intersection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import task1.Droid;
import task1.PriorityQueue;
import task1.Ship;

public class Main {

  public static void main(String[] args) {

    tasl1();
    tasl2();
    //task 2 to be continued
    //task 3 to be continued

  }


  private static void tasl2() {
    String[] stringArray1 = {"hello", "to", "you"};
    String[] stringArray2 = {"hello", "to", "me"};
    int[] intArray = {1, 2, 3, 44, 1, 44, 3, 78, 78};

//    System.out.println(intersection(stringArray1, stringArray2));
//    System.out.println(difference(stringArray1, stringArray2));
//    System.out.println(difference(stringArray2, stringArray1));
    System.out.println(dellEqualNumbers(intArray));
  }


  private static void tasl1() {
    Droid d = new Droid("droid");
    Droid d1 = new Droid("droid1");
    Droid d2 = new Droid("droid2");

    Ship ship = new Ship(d);
    ship.putDroid(d2);
    ship.putDroid(d1);

    PriorityQueue priorityQueue = new PriorityQueue(d);
    priorityQueue.putDroid(d2);
    priorityQueue.putDroid(d1);

    System.out.println(ship.getDroid(1));
    System.out.println(ship.getAllDroids());
    System.out.println(priorityQueue.getAllDroids());
  }


}
