package task1;

import java.util.ArrayList;
import java.util.Collections;

public class PriorityQueue<T extends Droid> extends Ship {

  public PriorityQueue() {
  }

  public PriorityQueue(Droid droid) {
    super(droid);
  }

  public PriorityQueue(ArrayList droids) {
    super(droids);
  }

  @Override
  public Droid getDroid(int indexOfDroid) {
    Collections.sort(droids);
    return super.getDroid(indexOfDroid);
  }

  @Override
  public ArrayList getAllDroids() {
    Collections.sort(droids);
    return super.getAllDroids();
  }
}
