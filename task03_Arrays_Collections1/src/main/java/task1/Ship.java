package task1;

import java.util.ArrayList;

public class Ship<T extends Droid>  {
   ArrayList<T> droids = new ArrayList<T>();;

  public Ship() {
  }

  public Ship(T droid) {
    this.droids.add(droid);
  }

  public Ship(ArrayList<T> droids) {
    this.droids = droids;
  }

  public void putDroid(T droid) {
    this.droids.add(droid);
  }

  public T getDroid(int indexOfDroid) {
    return droids.get(indexOfDroid);
  }

  public ArrayList<T> getAllDroids() {
    return droids;
  }
}
