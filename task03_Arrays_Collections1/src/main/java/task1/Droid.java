package task1;

public class Droid implements Comparable<Droid>{
  private String name;
  private int id;
  private static int countOfObject = 0;

  public Droid(String name) {
    this.name = name;
    countOfObject++;
    id = countOfObject;
  }

  public int getId() {
    return id;
  }

  public static int getCountOfObject() {
    return countOfObject;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int compareTo(Droid droid) {
    return name.compareTo(droid.name);
  }

  @Override
  public String toString() {
    return "Droid{" +
        "name='" + name + '\'' +
        ", id=" + id +
        '}';
  }
}
