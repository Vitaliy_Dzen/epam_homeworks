package task2;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Task2 {

  public static Set<String> intersection(final String[] inputArray1, final String[] inputArray2) {
    Set<String> first = new HashSet<>(Arrays.asList(inputArray1));
    Set<String> second = new HashSet<>(Arrays.asList(inputArray2));
    first.retainAll(second);
    return first;
  }

  public static Set<String> difference(final String[] inputArray1, final String[] inputArray2) {
    Set<String> first = new HashSet<>(Arrays.asList(inputArray1));
    Set<String> second = new HashSet<>(Arrays.asList(inputArray2));
    first.removeAll(second);
    return first;
  }

  public static Set<Integer> dellEqualNumbers(final int [] inputArray){
//    Set<Integer> setIntegers = IntStream.of(inputArray).boxed().collect(Collectors.toSet());
    return IntStream.of(inputArray).boxed().collect(Collectors.toSet());
  }

}