public class Phone {

  @PhoneAnnotation("Model")
  private String nameOfModel;
  @PhoneAnnotation("Type")
  private String type;
  private double price;

  public Phone(String nameOfModel, String type, double price) {
    this.nameOfModel = nameOfModel;
    this.type = type;
    this.price = price;
  }

  public String getNameOfModel() {
    return nameOfModel;
  }

  public void setNameOfModel(String nameOfModel) {
    this.nameOfModel = nameOfModel;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public String toString() {
    return "Phone{" +
        "nameOfModel='" + nameOfModel + '\'' +
        ", type=" + type +
        ", price=" + price +
        '}';
  }
}